package factory;

import java.util.Date;

/**
 * Created by boehm on 24/01/17.
 */

public class Worker {
    protected String name;

    public Worker(String name) {
        super();
        this.name = name;
    }

    public Task Work(int start, int end, Date begining){
        String action = "Work";
        Factory factory = new Factory(action,this.name,start,end,begining);
        return factory.worker();
    }

    public Task other(int start, int end, Date begining){
        String action = "Other";
        Factory factory = new Factory(action,this.name,start,end,begining);
        return factory.worker();
    }
}