package factory;

/**
 * Created by boehm on 24/01/17.
 */

public class Task {
    protected Worker worker;
    protected Activity activity;

    public Task(Worker worker, Activity activity) {
        super();
        this.worker = worker;
        this.activity = activity;
    }


    public Worker getWorker() {
        return worker;
    }
    public void setWorker(Worker worker) {
        this.worker = worker;
    }
    public Activity getActivity() {
        return activity;
    }
    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}