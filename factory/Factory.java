package factory;

import java.util.Date;

/**
 * Created by boehm on 24/01/17.
 */

public class Factory {

    protected String action;
    protected String name;
    protected int start;
    protected int end;
    protected Date begining;


    public Factory(String action, String name, int start, int end, Date begining) {
        super();
        this.action = action;
        this.name = name;
        this.start = start;
        this.end = end;
        this.begining = begining;
    }

    public Task worker(){
        Worker worker = new Worker(name);
        Activity activity = new Activity(action, start, end, begining);
        Task task = new Task(worker,activity);

        return task;
    }

    public Task humanWorker(){
        HumanWorker humanWorker = new HumanWorker(name);
        Activity activity = new Activity(action, start, end, begining);
        Task task = new Task(humanWorker,activity);

        return task;
    }

    public Task robotWorker(){
        RobotWorker robotWorker = new RobotWorker(name);
        Activity activity = new Activity(action, start, end, begining);
        Task task = new Task(robotWorker,activity);

        return task;
    }

    public String buildindCSV(){
        String result = this.name + "," + this.action + "," + this.start + "," + this.end + "," + this.begining;
        return result;
    }

}
