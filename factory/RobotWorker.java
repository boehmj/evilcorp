package factory;

import java.util.Date;

/**
 * Created by boehm on 24/01/17.
 */

public class RobotWorker extends Worker {

    public RobotWorker(String name) {
        super(name);
    }

    public Task Standby(int start, int end, Date begining){
        String action = "Standby";
        Factory factory = new Factory(action,this.name,start,end,begining);
        return factory.worker();
    }

}