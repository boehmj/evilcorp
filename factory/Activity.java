package factory;

import java.util.Date;

/**
 * Created by boehm on 24/01/17.
 */

public class Activity {
    private String name;
    private int start;
    private int end;
    private Date begining;


    public Activity(String name, int start, int end, Date begining) {
        super();
        this.name = name;
        this.start = start;
        this.end = end;
        this.begining = begining;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getstart() {
        return start;
    }
    public void setstart(int start) {
        this.start = start;
    }
    public int getend() {
        return end;
    }
    public void setend(int end) {
        this.end = end;
    }
    public Date getBegining() {
        return begining;
    }
    public void setBegining(Date begining) {
        this.begining = begining;
    }

}