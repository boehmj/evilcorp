package factory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by boehm on 24/01/17.
 */
public class main {

    public static void main(String[] args){
        if(args.length == 7 && args[0].compareTo("--file")!=0){
            String date = args[6];
            SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd");
            try {
                Date dateType = sdf.parse(date);
                if(checkPoste(args[0].toLowerCase())){
                    HumanWorker humanWorker = new HumanWorker(args[1]);
                    HumanWorker.Work(Integer.parseInt(args[4]),
                            Integer.parseInt(args[5]),
                            dateType);
                }
                else {
                    RobotWorker robotWorker = new RobotWorker(args[1]);
                    robotWorker.Standby(Integer.parseInt(args[4]),
                            Integer.parseInt(args[5]),
                            dateType);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (args[0].compareTo("--file")==0) {

        }
    }

    public static boolean checkPoste(String poste){
        if(poste.compareTo("activity") == 0){
            return true;
        }
        return false;
    }
}
