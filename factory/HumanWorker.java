package factory;

import java.util.Date;

/**
 * Created by boehm on 24/01/17.
 */

public class HumanWorker extends Worker{

    public HumanWorker(String name) {
        super(name);
    }

    public Task Eat(int start, int end, Date begining){
        String action = "Eat";
        Factory factory = new Factory(this.name,action,start,end,begining);
        return factory.worker();
    }

    public Task Work(int start, int end, Date begining){
        String action = "Work";
        Factory factory = new Factory(this.name,action,start,end,begining);
        return factory.worker();
    }

    public Task Sleep(int start, int end, Date begining){
        String action = "Sleep";
        Factory factory = new Factory(this.name,action,start,end,begining);
        return factory.worker();
    }

}
