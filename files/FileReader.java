package files;

import java.io.*;

/**
 * Created by boehm on 24/01/17.
 */
public class FileReader {
    private File file;

    public FileReader(File file){
        this.file = file;
    }

    public String readFile(){
        String chaine = "";

        try{
            InputStream ips=new FileInputStream(file);
            InputStreamReader ipsr=new InputStreamReader(ips);
            BufferedReader br=new BufferedReader(ipsr);
            String ligne;
            while ((ligne=br.readLine())!=null){
                System.out.println(ligne);
                chaine+=ligne+"\n";
            }
            br.close();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        return chaine;
    }
}
